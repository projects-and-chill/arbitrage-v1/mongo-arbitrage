FROM mongo:4
COPY ./data/ /usr/share/data
COPY ./dump/ /usr/share/dump-arbitrage-db
COPY ./init-mongo.sh /docker-entrypoint-initdb.d/init-mongo.sh
COPY ./mongoimport.sh /docker-entrypoint-initdb.d/mongoimport.sh
